# Java - RPG Collaboration
A Java project where we were provided with a set of pre-made files we were supposed to refactor 
and add to match the task description.

#### Assumptions
Assumptions / Changes
- I changed the return type of "equipArmor()" & "equipWeapon" to type boolean as I
    think it makes sense to return true/false if the character successfully could
    equip the item, so the item could e.g. be removed from the drops list later on.
- I chose to make the factories static, as I do not see a good reason to instantiate a new
    factory every time I want to create something.
- I chose to replace the "damageType" variable in Character.takeDamage() from a String
    to an enum, as there are only two damage types and enums are less error prone.

## Description
This is just the base of the RPG game. See the GameLoop class for more in-depth information.

## Showcase
Small showcase on running the demonstration package.

### (Auto-generating heroes, abilities and equipping items)
![](gitlab-files/1.PNG)

### 1. Choosing a party size
![](gitlab-files/2.png)

### 2. Choose action
![](gitlab-files/3.png)

### 3. Perform ability
![](gitlab-files/4.png)

### 4. Equip new Armor
![](gitlab-files/5.png)

### 5. Equip new Weapon
![](gitlab-files/6.png)