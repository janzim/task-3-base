package main.java.characters;
// Imports

import main.java.characters.abstractions.AttackBladedWeapon;
import main.java.characters.abstractions.Melee;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.BladeWeapon;
import main.java.items.weapons.abstractions.WeaponType;

import static main.java.basestats.CharacterBaseStatsDefensive.*;
import static main.java.basestats.CharacterBaseStatsOffensive.WARRIOR_MELEE_ATTACK_POWER;

/**
 * Warriors are combat veterans, durable forces on the battlefield.
 * They are masters of the blade and wield it with unmatched ferocity.
 */
public class Warrior extends Melee implements AttackBladedWeapon {

    /**
     * Constructor
     */
    public Warrior() {
        super(WARRIOR_BASE_HEALTH, WARRIOR_BASE_PHYS_RED, WARRIOR_BASE_MAGIC_RES, WARRIOR_MELEE_ATTACK_POWER,
                ArmorFactory.getArmor(ArmorType.Plate, new Common()),         // starter Armor
                WeaponFactory.getItem(WeaponType.Axe, new Common()));         // starter Weapon
    }

    // Character behaviours

    /**
     * Damages the enemy
     */
    @Override
    public double attackWithBladedWeapon() {
        BladeWeapon bladeWeapon = (BladeWeapon) equippedWeapon;

        return getBaseAttackPower() * bladeWeapon.getAttackPowerModifier() * equippedWeapon.getRarity().powerModifier(); // Replaced with actual damage amount based on calculations
    }
}
