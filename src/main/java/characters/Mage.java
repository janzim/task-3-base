package main.java.characters;
// Imports

import main.java.characters.abstractions.Caster;
import main.java.characters.abstractions.DamagingSpellAbility;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.spells.abstractions.DamagingSpell;

import static main.java.basestats.CharacterBaseStatsDefensive.*;
import static main.java.basestats.CharacterBaseStatsOffensive.MAGE_MAGIC_POWER;

/**
 * Mages are masters of arcane magic. Their skills are honed after years of dedicated study.
 * They conjure arcane energy to deal large amounts of damage to enemies.
 * They are vulnerable to physical attacks but are resistant to magic.
 */
public class Mage extends Caster implements DamagingSpellAbility {
    // Metadata
    private DamagingSpell damagingSpell;

    /**
     * Constructor
     */
    public Mage() {
        super(MAGE_BASE_HEALTH, MAGE_BASE_PHYS_RED, MAGE_BASE_MAGIC_RES, MAGE_MAGIC_POWER,
                ArmorFactory.getArmor(ArmorType.Cloth, new Common()),          // starter Armor
                WeaponFactory.getItem(WeaponType.Wand, new Common()));         // starter Weapon
    }

    /**
     * Constructor
     *
     * @param damagingSpell which spell the Mage should equip
     */
    public Mage(DamagingSpell damagingSpell) {
        super(MAGE_BASE_HEALTH, MAGE_BASE_PHYS_RED, MAGE_BASE_MAGIC_RES, MAGE_MAGIC_POWER,
                ArmorFactory.getArmor(ArmorType.Cloth, new Common()),         // starter Armor
                WeaponFactory.getItem(WeaponType.Wand, new Common()));         // starter Weapon
        this.damagingSpell = damagingSpell;
    }

    // Character behaviours

    /**
     * Damages the enemy with its spells
     *
     * @return the amount of damage done
     */
    @Override
    public double castDamagingSpell() {
        MagicWeapon magicWeapon = (MagicWeapon) equippedWeapon;

        return getBaseMagicPower() * magicWeapon.getMagicPowerModifier() * damagingSpell.getSpellDamageModifier(); // Replaced with actual damage amount based on calculations
    }
}
