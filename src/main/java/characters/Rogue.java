package main.java.characters;
// Imports

import main.java.characters.abstractions.AttackBladedWeapon;
import main.java.characters.abstractions.Melee;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.BladeWeapon;
import main.java.items.weapons.abstractions.RangedWeapon;
import main.java.items.weapons.abstractions.WeaponType;

import static main.java.basestats.CharacterBaseStatsDefensive.*;
import static main.java.basestats.CharacterBaseStatsOffensive.ROGUE_MELEE_ATTACK_POWER;

/**
 * Rogues are stealthy combatants of the shadows.
 * They wield bladed weapons with great agility and dispatch their enemies swiftly.
 */
public class Rogue extends Melee implements AttackBladedWeapon {

    /**
     * Constructor
     */
    public Rogue() {
        super(ROGUE_BASE_HEALTH, ROGUE_BASE_PHYS_RED, ROGUE_BASE_MAGIC_RES, ROGUE_MELEE_ATTACK_POWER,
                ArmorFactory.getArmor(ArmorType.Leather, new Common()),         // starter Armor
                WeaponFactory.getItem(WeaponType.Sword, new Common()));         // starter Weapon
    }

    // Character behaviours

    /**
     * Damages the enemy with a blade weapon
     */
    @Override
    public double attackWithBladedWeapon() {
        BladeWeapon bladeWeapon = (BladeWeapon) equippedWeapon;

        return getBaseAttackPower() * bladeWeapon.getAttackPowerModifier() * equippedWeapon.getRarity().powerModifier(); // Replaced with actual damage amount based on calculations
    }
}
