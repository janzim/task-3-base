package main.java.characters;
// Imports

import main.java.characters.abstractions.Caster;
import main.java.characters.abstractions.DamagingSpellAbility;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.spells.abstractions.DamagingSpell;

import static main.java.basestats.CharacterBaseStatsDefensive.*;
import static main.java.basestats.CharacterBaseStatsOffensive.WARLOCK_MAGIC_POWER;

/**
 * Warlocks are masters of chaos, entropy, and death. They were once mages but were corrupted by power.
 * They can conjure up pure chaos energy to destroy their enemies.
 */
public class Warlock extends Caster implements DamagingSpellAbility {
    // Metadata
    private DamagingSpell damagingSpell;

    /**
     * Constructor
     */
    public Warlock() {
        super(WARLOCK_BASE_HEALTH, WARLOCK_BASE_PHYS_RED, WARLOCK_BASE_MAGIC_RES, WARLOCK_MAGIC_POWER,
                ArmorFactory.getArmor(ArmorType.Cloth, new Common()),         // starter Armor
                WeaponFactory.getItem(WeaponType.Staff, new Common()));         // starter Weapon
    }

    /**
     * Constructor
     *
     * @param damagingSpell the damaging spell to attach to this warlock
     */
    public Warlock(DamagingSpell damagingSpell) {
        super(WARLOCK_BASE_HEALTH, WARLOCK_BASE_PHYS_RED, WARLOCK_BASE_MAGIC_RES, WARLOCK_MAGIC_POWER,
                ArmorFactory.getArmor(ArmorType.Cloth, new Common()),         // starter Armor
                WeaponFactory.getItem(WeaponType.Staff, new Common()));         // starter Weapon
        this.damagingSpell = damagingSpell;
    }

    // Character behaviours

    /**
     * Damages the enemy with its spells
     */
    @Override
    public double castDamagingSpell() {
        MagicWeapon magicWeapon = (MagicWeapon) equippedWeapon;

        return getBaseMagicPower() * magicWeapon.getMagicPowerModifier() * damagingSpell.getSpellDamageModifier(); // Replaced with actual damage amount based on calculations
    }
}
