package main.java.characters;
// Imports

import main.java.characters.abstractions.ShieldAbility;
import main.java.characters.abstractions.Support;
import main.java.consolehelpers.Color;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.spells.abstractions.ShieldingSpell;

import static main.java.basestats.CharacterBaseStatsDefensive.*;

/**
 * Priest are the servants of the light and goodness.
 * They use holy magic to heal and shield allies.
 * As a support class they only have defensive stats.
 */
public class Priest extends Support implements ShieldAbility {
    // Metadata
    private ShieldingSpell shieldingSpell;

    /**
     * Constructor
     */
    public Priest() {
        super(PRIEST_BASE_HEALTH, PRIEST_BASE_PHYS_RED, PRIEST_BASE_MAGIC_RES,
                ArmorFactory.getArmor(ArmorType.Cloth, new Common()),         // starter Armor
                WeaponFactory.getItem(WeaponType.Wand, new Common()));         // starter Weapon
    }

    /**
     * Constructor
     *
     * @param shieldingSpell which spell the Priest should equip
     */
    public Priest(ShieldingSpell shieldingSpell) {
        super(PRIEST_BASE_HEALTH, PRIEST_BASE_PHYS_RED, PRIEST_BASE_MAGIC_RES,
                ArmorFactory.getArmor(ArmorType.Cloth, new Common()),         // starter Armor
                WeaponFactory.getItem(WeaponType.Wand, new Common()));         // starter Weapon
        this.shieldingSpell = shieldingSpell;
    }

    // Character behaviours

    /**
     * Shields a party member for a percentage of their maximum health.
     *
     * @param partyMemberMaxHealth the max health of the party member to shield
     */
    @Override
    public double shieldPartyMember(double partyMemberMaxHealth) {
        double shieldingDone = (partyMemberMaxHealth * shieldingSpell.getAbsorbShieldPercentage()) +
                (((MagicWeapon) equippedWeapon).getMagicPowerModifier()
                        * equippedWeapon.getRarity().powerModifier());
        return shieldingDone;
    }
}
