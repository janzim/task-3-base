package main.java.characters;
// Imports

import main.java.characters.abstractions.HealAbility;
import main.java.characters.abstractions.Support;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.spells.abstractions.HealingSpell;

import static main.java.basestats.CharacterBaseStatsDefensive.*;

/**
 * Druids are spell casters who use nature based magic to aid their allies in battle.
 * They can heal their allies or protect them using the forces of nature.
 * As a support class they only have defensive stats.
 */
public class Druid extends Support implements HealAbility {
    // Metadata
    private HealingSpell healingSpell;

    /**
     * Constructor
     */
    public Druid() {
        super(DRUID_BASE_HEALTH, DRUID_BASE_PHYS_RED, DRUID_BASE_MAGIC_RES,
                ArmorFactory.getArmor(ArmorType.Leather, new Common()),         // starter Armor
                WeaponFactory.getItem(WeaponType.Staff, new Common()));         // starter Weapon
    }

    /**
     * Constructor
     *
     * @param healingSpell which spell the Druid should equip
     */
    public Druid(HealingSpell healingSpell) {
        super(DRUID_BASE_HEALTH, DRUID_BASE_PHYS_RED, DRUID_BASE_MAGIC_RES,
                ArmorFactory.getArmor(ArmorType.Leather, new Common()),         // starter Armor
                WeaponFactory.getItem(WeaponType.Staff, new Common()));         // starter Weapon
        this.healingSpell = healingSpell;
    }

    // Character behaviours

    /**
     * Heals the party member
     *
     * @return healing done
     */
    @Override
    public double healPartyMember() {
        double healingDone = healingSpell.getHealingAmount() *
                ((MagicWeapon) equippedWeapon).getMagicPowerModifier() *
                equippedWeapon.getRarity().powerModifier();
        return healingDone;
    }
}
