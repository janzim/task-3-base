package main.java.characters.abstractions;

/**
 * Character Ability to attack with ranged weapons
 */
public interface AttackRangedWeapon {
    /**
     * Perform the attack
     *
     * @return amount of damage dealt
     */
    double attackWithRangedWeapon();
}
