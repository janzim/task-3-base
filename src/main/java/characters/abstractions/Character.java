package main.java.characters.abstractions;

import main.java.items.armor.abstractions.Armor;
import main.java.items.weapons.abstractions.Weapon;

/**
 * Base class for any Hero
 */
public abstract class Character {
    // Active trackers and flags
    protected double currentHealth;
    protected boolean isDead = false;

    // Metadata
    protected Armor equippedArmor;
    protected Weapon equippedWeapon;

    // Base stats defensive
    protected double baseHealth;
    protected double basePhysReductionPercent; // Armor
    protected double baseMagicReductionPercent; // Magic armor

    /**
     * Constructor
     *
     * @param baseHealth                the health to start with
     * @param basePhysReductionPercent  the physical reduction percent (in decimals) to start with
     * @param baseMagicReductionPercent the magic reduction percent (in decimals) to start with
     * @param equippedArmor             the armor to start with (armor category cannot be changed after!)
     * @param equippedWeapon            the weapon to start with (weapon category cannot be changed after!)
     */
    public Character(double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent, Armor equippedArmor, Weapon equippedWeapon) {
        this.baseHealth = baseHealth;
        this.basePhysReductionPercent = basePhysReductionPercent;
        this.baseMagicReductionPercent = baseMagicReductionPercent;
        this.equippedArmor = equippedArmor;
        this.equippedWeapon = equippedWeapon;
        // When the character is created it has base health
        this.currentHealth = baseHealth;
    }

    /**
     * Get the current max health of a hero
     *
     * @return the max health of hero
     */
    public double getCurrentMaxHealth() {
        return baseHealth * equippedArmor.getHealthModifier() * equippedArmor.getRarityModifier().powerModifier();
    }

    /**
     * Get the current health of a hero
     *
     * @return the current health
     */
    public double getCurrentHealth() {
        return currentHealth;
    }

    /**
     * Get if the hero is dead
     *
     * @return true if hero is dead, false otherwise
     */
    public Boolean getDead() {
        return isDead;
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     *
     * @param newArmor
     */
    public boolean equipArmor(Armor newArmor) {
        if (equippedArmor.getClass() == newArmor.getClass()) {
            equippedArmor = newArmor;
            return true;
        }
        return false;
    }

    /**
     * Equips a weapon to the character, modifying stats.
     *
     * @param newWeapon
     * @return if user
     */
    public boolean equipWeapon(Weapon newWeapon) {
        if (equippedWeapon.getClass().getSuperclass() == newWeapon.getClass().getSuperclass()) {
            equippedWeapon = newWeapon;
            return true;
        }
        return false;
    }

    /**
     * Takes damage from an enemy's attack.
     *
     * @param incomingDamage the amount of damage dealt to this character
     * @Param damageType if attack was physical or magic
     */
    public double takeDamage(double incomingDamage, DamageType damageType) {
        double totalDamageTaken = 0;

        if (damageType == DamageType.Physical) {
            totalDamageTaken = incomingDamage * (1 - basePhysReductionPercent * equippedArmor.getPhysRedModifier() * equippedArmor.getRarityModifier().powerModifier());
        } else if (damageType == DamageType.Magic) {
            totalDamageTaken = incomingDamage * (1 - baseMagicReductionPercent * equippedArmor.getMagicRedModifier() * equippedArmor.getRarityModifier().powerModifier());
        }

        return totalDamageTaken < 1.0 ? 1 : totalDamageTaken; // Return damage taken after damage reductions, based on type of damage.
    }

    /**
     * Get the current equipped armor of hero
     *
     * @return the equipped armor
     */
    public Armor getEquippedArmor() {
        return equippedArmor;
    }

    /**
     * Get the current equipped weapon of hero
     *
     * @return the equipped weapon
     */
    public Weapon getEquippedWeapon() {
        return equippedWeapon;
    }
}
