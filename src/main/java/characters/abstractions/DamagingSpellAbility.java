package main.java.characters.abstractions;

/**
 * Character Ability to attack with a damage spell
 */
public interface DamagingSpellAbility {
    /**
     * Perform the spell attack
     *
     * @return amount of damage dealt
     */
    double castDamagingSpell();
}
