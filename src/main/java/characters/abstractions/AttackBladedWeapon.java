package main.java.characters.abstractions;

/**
 * Character Ability to attack with bladed weapons
 */
public interface AttackBladedWeapon {
    /**
     * Perform the attack
     *
     * @return amount of damage dealt
     */
    double attackWithBladedWeapon();
}
