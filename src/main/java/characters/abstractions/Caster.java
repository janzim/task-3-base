package main.java.characters.abstractions;

import main.java.items.armor.abstractions.Armor;
import main.java.items.weapons.abstractions.Weapon;

/**
 * Sub-Base class for Heroes who use magic spells
 */
public abstract class Caster extends Character {
    private double baseMagicPower;

    /**
     * Constructor
     *
     * @param baseHealth                the health to start with
     * @param basePhysReductionPercent  the physical reduction percent (in decimals) to start with
     * @param baseMagicReductionPercent the magic reduction percent (in decimals) to start with
     * @param baseMagicPower            the magic power to start with
     * @param armor                     the armor to start with (armor category cannot be changed after!)
     * @param weapon                    the weapon to start with (weapon category cannot be changed after!)
     */
    public Caster(double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent, double baseMagicPower, Armor armor, Weapon weapon) {
        super(baseHealth, basePhysReductionPercent, baseMagicReductionPercent, armor, weapon);
        this.baseMagicPower = baseMagicPower;
    }

    /**
     * Get the magic power this hero starts with
     *
     * @return the base magic power
     */
    public double getBaseMagicPower() {
        return baseMagicPower;
    }
}
