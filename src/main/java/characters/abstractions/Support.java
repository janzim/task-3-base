package main.java.characters.abstractions;

import main.java.items.armor.abstractions.Armor;
import main.java.items.weapons.abstractions.Weapon;

/**
 * Sub-Base class for Heroes who support other heroes
 */
public abstract class Support extends Character {

    /**
     * Constructor
     *
     * @param baseHealth                the health to start with
     * @param basePhysReductionPercent  the physical reduction percent (in decimals) to start with
     * @param baseMagicReductionPercent the magic reduction percent (in decimals) to start with
     * @param armor                     the armor to start with (armor category cannot be changed after!)
     * @param weapon                    the weapon to start with (weapon category cannot be changed after!)
     */
    public Support(double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent, Armor armor, Weapon weapon) {
        super(baseHealth, basePhysReductionPercent, baseMagicReductionPercent, armor, weapon);
    }
}
