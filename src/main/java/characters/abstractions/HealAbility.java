package main.java.characters.abstractions;

/**
 * Character Ability to heal other party members
 */
public interface HealAbility {

    /**
     * Perform the spell to heal
     *
     * @return amount of points healed
     */
    double healPartyMember();
}
