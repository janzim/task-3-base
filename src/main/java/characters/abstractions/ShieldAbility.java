package main.java.characters.abstractions;

public interface ShieldAbility {

    /**
     * Perform the shield spell
     *
     * @param partyMemberMaxHealth The max health of shielded partymember
     * @return amount of points shielded
     */
    double shieldPartyMember(double partyMemberMaxHealth);
}
