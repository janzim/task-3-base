package main.java.characters.abstractions;

/**
 * Character Ability to attack with blunt weapons
 */
public interface AttackBluntWeaponAbility {
    /**
     * Perform the attack
     *
     * @return amount of damage dealt
     */
    double attackWithBluntWeapon();
}
