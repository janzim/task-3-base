package main.java.characters.abstractions;

import main.java.items.armor.abstractions.Armor;
import main.java.items.weapons.abstractions.Weapon;

/**
 * Sub-Base class for Heroes who use melee
 */
public abstract class Melee extends Character {
    private double baseAttackPower;

    /**
     * Constructor
     *
     * @param baseHealth                the health to start with
     * @param basePhysReductionPercent  the physical reduction percent (in decimals) to start with
     * @param baseMagicReductionPercent the magic reduction percent (in decimals) to start with
     * @param baseAttackPower           the attack power to start with
     * @param armor                     the armor to start with (armor category cannot be changed after!)
     * @param weapon                    the weapon to start with (weapon category cannot be changed after!)
     */
    public Melee(double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent, double baseAttackPower, Armor armor, Weapon weapon) {
        super(baseHealth, basePhysReductionPercent, baseMagicReductionPercent, armor, weapon);
        this.baseAttackPower = baseAttackPower;
    }

    /**
     * Get the attack power this hero starts with
     *
     * @return the base attack power
     */
    public double getBaseAttackPower() {
        return baseAttackPower;
    }
}
