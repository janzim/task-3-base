package main.java.characters.abstractions;

/**
 * List of all possible types of damage
 */
public enum DamageType {
    Physical,
    Magic
}
