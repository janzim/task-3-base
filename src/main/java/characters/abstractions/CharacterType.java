package main.java.characters.abstractions;

/**
 * List of all available heroes
 */
public enum CharacterType {
    Druid,
    Mage,
    Paladin,
    Priest,
    Ranger,
    Rogue,
    Warlock,
    Warrior
}
