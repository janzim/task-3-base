package main.java.characters;
// Imports

import main.java.characters.abstractions.AttackBluntWeaponAbility;
import main.java.characters.abstractions.Melee;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.BluntWeapon;
import main.java.items.weapons.abstractions.WeaponType;

import static main.java.basestats.CharacterBaseStatsDefensive.*;
import static main.java.basestats.CharacterBaseStatsOffensive.PALADIN_MELEE_ATTACK_POWER;

/**
 * Paladins are faithful servants of the light and everything holy.
 * They dispatch justice and are filled with vengeance for the wrongs done to society by evil.
 * Paladins are very durable and typically wield heavy weapons.
 */
public class Paladin extends Melee implements AttackBluntWeaponAbility {
    /**
     * Constructor
     */
    public Paladin() {
        super(PALADIN_BASE_HEALTH, PALADIN_BASE_PHYS_RED, PALADIN_BASE_MAGIC_RES, PALADIN_MELEE_ATTACK_POWER,
                ArmorFactory.getArmor(ArmorType.Plate, new Common()),         // starter Armor
                WeaponFactory.getItem(WeaponType.Hammer, new Common()));         // starter Weapon
    }

    // Character behaviours

    /**
     * Damages the enemy
     */
    @Override
    public double attackWithBluntWeapon() {
        BluntWeapon bluntWeapon = (BluntWeapon) equippedWeapon;

        return getBaseAttackPower() * bluntWeapon.getAttackPowerModifier() * equippedWeapon.getRarity().powerModifier(); // Replaced with actual damage amount based on calculations
    }
}
