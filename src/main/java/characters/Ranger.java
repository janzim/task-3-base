package main.java.characters;
// Imports

import main.java.characters.abstractions.AttackRangedWeapon;
import main.java.characters.abstractions.Ranged;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.BluntWeapon;
import main.java.items.weapons.abstractions.RangedWeapon;
import main.java.items.weapons.abstractions.WeaponType;

import static main.java.basestats.CharacterBaseStatsDefensive.*;
import static main.java.basestats.CharacterBaseStatsOffensive.RANGER_RANGED_ATTACK_POWER;

/**
 * Rangers
 * Are masters of ranged combat. They use a wide arsenal of weapons to dispatch enemies.
 */
public class Ranger extends Ranged implements AttackRangedWeapon {
    /**
     * Constructor
     */
    public Ranger() {
        super(RANGER_BASE_HEALTH, RANGER_BASE_PHYS_RED, RANGER_BASE_MAGIC_RES, RANGER_RANGED_ATTACK_POWER,
                ArmorFactory.getArmor(ArmorType.Mail, new Common()),         // starter Armor
                WeaponFactory.getItem(WeaponType.Bow, new Common()));         // starter Weapon
    }

    // Character behaviours

    /**
     * Damages the enemy
     */
    @Override
    public double attackWithRangedWeapon() {
        RangedWeapon rangedWeapon = (RangedWeapon) equippedWeapon;

        return getBaseAttackPower() * rangedWeapon.getAttackPowerModifier() * equippedWeapon.getRarity().powerModifier(); // Replaced with actual damage amount based on calculations
    }
}
