package main.java;

import main.java.demonstrationHelpers.Demonstration;

/**
 * Assumptions / Changes
 * - I changed the return type of "equipArmor()" & "equipWeapon" to type boolean as I
 * think it makes sense to return true/false if the character successfully could
 * equip the item, so the item could e.g. be removed from the drops list later on.
 * - I chose to make the factories static, as I do not see a good reason to instantiate a new
 * factory every time I want to create something.
 * - I chose to replace the "damageType" variable in Character.takeDamage() from a String
 * to an enum, as there are only two damage types and enums are less error prone.
 */

public class Main {
    public static void main(String[] args) {
        Demonstration demo = new Demonstration();
        demo.runDemo();
    }
}
