package main.java.factories;
// Imports

import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.items.weapons.magic.Staff;
import main.java.items.weapons.magic.Wand;
import main.java.items.weapons.melee.*;
import main.java.items.weapons.ranged.Bow;
import main.java.items.weapons.ranged.Crossbow;
import main.java.items.weapons.ranged.Gun;

/**
 * This factory exists to be responsible for creating new weapons.
 */
public class WeaponFactory {

    /**
     * Generate a new weapon
     *
     * @param weaponType the type of weapon to create
     * @param rarity     the rarity of the weapon
     * @return the created Weapon Object
     */
    public static Weapon getItem(WeaponType weaponType, Rarity rarity) {
        switch (weaponType) {
            case Axe:
                return new Axe(rarity);
            case Bow:
                return new Bow(rarity);
            case Crossbow:
                return new Crossbow(rarity);
            case Dagger:
                return new Dagger(rarity);
            case Gun:
                return new Gun(rarity);
            case Hammer:
                return new Hammer(rarity);
            case Mace:
                return new Mace(rarity);
            case Staff:
                return new Staff(rarity);
            case Sword:
                return new Sword(rarity);
            case Wand:
                return new Wand(rarity);
            default:
                return null;
        }
    }
}
