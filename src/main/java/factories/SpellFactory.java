package main.java.factories;

import main.java.spells.abstractions.Spell;
import main.java.spells.abstractions.SpellType;
import main.java.spells.damaging.ArcaneMissile;
import main.java.spells.damaging.ChaosBolt;
import main.java.spells.healing.Regrowth;
import main.java.spells.healing.SwiftMend;
import main.java.spells.shielding.Barrier;
import main.java.spells.shielding.Rapture;

/**
 * This factory exists to be responsible for creating new spells.
 */
public class SpellFactory {

    /**
     * Generate new spells
     *
     * @param spellType the type of spell to generate
     * @return the new Spell Object
     */
    public static Spell getSpells(SpellType spellType) {
        switch (spellType) {
            case ArcaneMissile:
                return new ArcaneMissile();
            case ChaosBolt:
                return new ChaosBolt();
            case Regrowth:
                return new Regrowth();
            case SwiftMend:
                return new SwiftMend();
            case Barrier:
                return new Barrier();
            case Rapture:
                return new Rapture();
            default:
                return null;
        }
    }
}
