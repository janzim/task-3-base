package main.java.factories;
// Imports

import main.java.items.armor.Cloth;
import main.java.items.armor.Leather;
import main.java.items.armor.Mail;
import main.java.items.armor.Plate;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.abstractions.Rarity;

/**
 * This factory exists to be responsible for creating new Armor.
 */
public class ArmorFactory {
    /**
     * Get a specific armor
     *
     * @param armorType          the type of armor to generate
     * @param itemRarityModifier the armor rarity type (e.g. Common, Uncommon, ...)
     * @return an Armor Object
     */
    public static Armor getArmor(ArmorType armorType, Rarity itemRarityModifier) {
        switch (armorType) {
            case Cloth:
                return new Cloth(itemRarityModifier);
            case Leather:
                return new Leather(itemRarityModifier);
            case Mail:
                return new Mail(itemRarityModifier);
            case Plate:
                return new Plate(itemRarityModifier);
            default:
                return null;
        }
    }
}
