package main.java.factories;

import main.java.characters.*;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.CharacterType;

// Imports

/**
 * This factory exists to be responsible for creating new Characters.
 */
public class CharacterFactory {

    /**
     * Get a specific character
     *
     * @param characterType the type of character to create
     * @return a Character Object
     */
    public static Character getCharacter(CharacterType characterType) {
        switch (characterType) {
            case Druid:
                return new Druid();
            case Mage:
                return new Mage();
            case Paladin:
                return new Paladin();
            case Priest:
                return new Priest();
            case Ranger:
                return new Ranger();
            case Rogue:
                return new Rogue();
            case Warlock:
                return new Warlock();
            case Warrior:
                return new Warrior();
            default:
                return null;
        }
    }
}
