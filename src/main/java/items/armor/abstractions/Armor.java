package main.java.items.armor.abstractions;

import main.java.items.rarity.abstractions.Rarity;

/**
 * Base armor class
 */
public abstract class Armor {
    // Stat modifiers
    protected double healthModifier;
    protected double physRedModifier;
    protected double magicRedModifier;
    // Rarity
    private Rarity rarityModifier;

    /**
     * Constructor
     *
     * @param healthModifier   health modifier of armor
     * @param physRedModifier  the physical reduction percent
     * @param magicRedModifier the magical reduction percent
     * @param rarityModifier   how rare the armor is
     */
    public Armor(double healthModifier, double physRedModifier, double magicRedModifier, Rarity rarityModifier) {
        this.healthModifier = healthModifier;
        this.physRedModifier = physRedModifier;
        this.magicRedModifier = magicRedModifier;
        this.rarityModifier = rarityModifier;
    }

    /**
     * Get the health modifier value
     *
     * @return health modifier
     */
    public double getHealthModifier() {
        return healthModifier;
    }

    /**
     * Get the physical reduction modifier value
     *
     * @return physical reduction modifier
     */
    public double getPhysRedModifier() {
        return physRedModifier;
    }

    /**
     * Get the magical reduction modifier value
     *
     * @return magical reduction modifier
     */
    public double getMagicRedModifier() {
        return magicRedModifier;
    }

    /**
     * Get the rarity modifier value
     *
     * @return rarity modifier class
     */
    public Rarity getRarityModifier() {
        return rarityModifier;
    }
}
