package main.java.items.armor.abstractions;

/**
 * List of all available armor types
 */
public enum ArmorType {
    Cloth,
    Leather,
    Mail,
    Plate
}
