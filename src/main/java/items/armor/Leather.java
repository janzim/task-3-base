package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;
import main.java.items.rarity.abstractions.Rarity;

/**
 * Leather armor, still kindof weak
 */
public class Leather extends Armor {
    // Constructors
    public Leather(Rarity itemRarity) {
        super(ArmorStatsModifiers.LEATHER_HEALTH_MODIFIER, ArmorStatsModifiers.LEATHER_PHYS_RED_MODIFIER, ArmorStatsModifiers.LEATHER_MAGIC_RED_MODIFIER, itemRarity);
    }
}
