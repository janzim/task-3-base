package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;
import main.java.items.rarity.abstractions.Rarity;

/**
 * Plate armor, just the best for the best
 */
public class Plate extends Armor {
    // Constructors
    public Plate(Rarity itemRarity) {
        super(ArmorStatsModifiers.PLATE_HEALTH_MODIFIER, ArmorStatsModifiers.PLATE_PHYS_RED_MODIFIER, ArmorStatsModifiers.PLATE_MAGIC_RED_MODIFIER, itemRarity);
    }
}
