package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.MagicWeapon;

public class Staff extends MagicWeapon {
    // Constructors
    public Staff() {
        super(WeaponStatsModifiers.STAFF_MAGIC_MOD);
    }

    public Staff(Rarity rarity) {
        super(WeaponStatsModifiers.STAFF_MAGIC_MOD, rarity);
    }
}
