package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.MagicWeapon;

public class Wand extends MagicWeapon {
    // Constructors
    public Wand() {
        super(WeaponStatsModifiers.WAND_MAGIC_MOD);
    }

    public Wand(Rarity rarity) {
        super(WeaponStatsModifiers.WAND_MAGIC_MOD, rarity);
    }
}
