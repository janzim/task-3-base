package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.RangedWeapon;

public class Crossbow extends RangedWeapon {
    // Constructors
    public Crossbow() {
        super(WeaponStatsModifiers.CROSSBOW_ATTACK_MOD);
    }

    public Crossbow(Rarity rarity) {
        super(WeaponStatsModifiers.CROSSBOW_ATTACK_MOD, rarity);
    }
}
