package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.RangedWeapon;

public class Gun extends RangedWeapon {
    // Constructors
    public Gun() {
        super(WeaponStatsModifiers.GUN_ATTACK_MOD);
    }

    public Gun(Rarity rarity) {
        super(WeaponStatsModifiers.GUN_ATTACK_MOD, rarity);
    }
}
