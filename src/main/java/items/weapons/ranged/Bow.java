package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.RangedWeapon;

public class Bow extends RangedWeapon {
    // Stat modifiers
    private double attackPowerModifier = WeaponStatsModifiers.BOW_ATTACK_MOD;

    // Constructors
    public Bow() {
        super(WeaponStatsModifiers.BOW_ATTACK_MOD);
    }

    public Bow(Rarity rarity) {
        super(WeaponStatsModifiers.BOW_ATTACK_MOD, rarity);
    }
}
