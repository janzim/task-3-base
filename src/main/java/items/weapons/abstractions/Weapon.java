package main.java.items.weapons.abstractions;

import main.java.items.rarity.Uncommon;
import main.java.items.rarity.abstractions.Rarity;

/**
 * Weapon Base Class
 */
public abstract class Weapon {
    // Rarity
    private final Rarity rarity;

    public Weapon() {
        this.rarity = new Uncommon();
    }

    public Weapon(Rarity rarity) {
        this.rarity = rarity;
    }

    // Public properties
    public Rarity getRarity() {
        return rarity;
    }
}
