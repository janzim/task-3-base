package main.java.items.weapons.abstractions;

import main.java.items.rarity.abstractions.Rarity;

/**
 * Ranged Weapon
 */
public abstract class RangedWeapon extends Weapon {
    // stat modifiers
    protected double attackPowerModifier;

    public RangedWeapon(double attackPowerModifier) {
        this.attackPowerModifier = attackPowerModifier;
    }

    public RangedWeapon(double attackPowerModifier, Rarity rarity) {
        super(rarity);
        this.attackPowerModifier = attackPowerModifier;
    }

    public double getAttackPowerModifier() {
        return attackPowerModifier;
    }
}
