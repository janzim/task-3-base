package main.java.items.weapons.abstractions;

import main.java.items.rarity.abstractions.Rarity;

/**
 * Blade weapon
 */
public abstract class BladeWeapon extends Weapon {
    // stat modifiers
    protected double attackPowerModifier;

    public BladeWeapon(double attackPowerModifier) {
        this.attackPowerModifier = attackPowerModifier;
    }

    public BladeWeapon(double attackPowerModifier, Rarity rarity) {
        super(rarity);
        this.attackPowerModifier = attackPowerModifier;
    }

    public double getAttackPowerModifier() {
        return attackPowerModifier;
    }
}
