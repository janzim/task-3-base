package main.java.items.weapons.abstractions;

import main.java.items.rarity.abstractions.Rarity;

/**
 * Magic Weapon
 */
public abstract class MagicWeapon extends Weapon {
    // stat modifiers
    protected double magicPowerModifier;

    public MagicWeapon(double magicPowerModifier) {
        this.magicPowerModifier = magicPowerModifier;
    }

    public MagicWeapon(double magicPowerModifier, Rarity rarity) {
        super(rarity);
        this.magicPowerModifier = magicPowerModifier;
    }

    public double getMagicPowerModifier() {
        return magicPowerModifier;
    }
}
