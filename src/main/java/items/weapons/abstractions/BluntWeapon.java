package main.java.items.weapons.abstractions;

import main.java.items.rarity.abstractions.Rarity;

/**
 * Blunt weapon
 */
public abstract class BluntWeapon extends Weapon {
    // stat modifiers
    protected double attackPowerModifier;

    public BluntWeapon(double attackPowerModifier) {
        this.attackPowerModifier = attackPowerModifier;
    }

    public BluntWeapon(double attackPowerModifier, Rarity rarity) {
        super(rarity);
        this.attackPowerModifier = attackPowerModifier;
    }

    public double getAttackPowerModifier() {
        return attackPowerModifier;
    }
}
