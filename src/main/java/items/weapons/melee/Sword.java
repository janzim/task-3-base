package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.BladeWeapon;

public class Sword extends BladeWeapon {
    // Constructors
    public Sword() {
        super(WeaponStatsModifiers.SWORD_ATTACK_MOD);
    }

    public Sword(Rarity rarity) {
        super(WeaponStatsModifiers.SWORD_ATTACK_MOD, rarity);
    }
}
