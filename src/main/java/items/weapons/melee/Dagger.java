package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.BladeWeapon;

public class Dagger extends BladeWeapon {
    // Constructors
    public Dagger() {
        super(WeaponStatsModifiers.DAGGER_ATTACK_MOD);
    }

    public Dagger(Rarity rarity) {
        super(WeaponStatsModifiers.DAGGER_ATTACK_MOD, rarity);
    }
}
