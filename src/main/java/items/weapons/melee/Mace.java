package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.BluntWeapon;

public class Mace extends BluntWeapon {
    // Constructors
    public Mace() {
        super(WeaponStatsModifiers.MACE_ATTACK_MOD);
    }

    public Mace(Rarity rarity) {
        super(WeaponStatsModifiers.MACE_ATTACK_MOD, rarity);
    }
}
