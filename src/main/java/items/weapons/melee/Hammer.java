package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.BluntWeapon;

public class Hammer extends BluntWeapon {
    // Constructors
    public Hammer() {
        super(WeaponStatsModifiers.HAMMER_ATTACK_MOD);
    }

    public Hammer(Rarity rarity) {
        super(WeaponStatsModifiers.HAMMER_ATTACK_MOD, rarity);
    }
}
