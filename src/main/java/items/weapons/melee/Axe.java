package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.BladeWeapon;

public class Axe extends BladeWeapon {
    // Constructors
    public Axe() {
        super(WeaponStatsModifiers.AXE_ATTACK_MOD);
    }

    public Axe(Rarity rarity) {
        super(WeaponStatsModifiers.AXE_ATTACK_MOD, rarity);
    }
}
