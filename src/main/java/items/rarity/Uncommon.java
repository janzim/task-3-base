package main.java.items.rarity;

import main.java.consolehelpers.Color;
import main.java.items.rarity.abstractions.Rarity;

import static main.java.basestats.ItemRarityModifiers.UNCOMMON_RARITY_MODIFIER;


/**
 * Uncommon rarity, a tad better than Common
 */
public class Uncommon implements Rarity {

    @Override
    public double powerModifier() {
        return UNCOMMON_RARITY_MODIFIER;
    }

    @Override
    public String getItemRarityName() {
        return "Uncommon";
    }

    @Override
    public String getItemRarityColor() {
        return Color.GREEN;
    }
}
