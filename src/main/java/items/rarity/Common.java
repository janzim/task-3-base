package main.java.items.rarity;

import main.java.consolehelpers.Color;
import main.java.items.rarity.abstractions.Rarity;

import static main.java.basestats.ItemRarityModifiers.COMMON_RARITY_MODIFIER;

/**
 * Common rarity, nothing special
 */
public class Common implements Rarity {

    @Override
    public double powerModifier() {
        return COMMON_RARITY_MODIFIER;
    }

    @Override
    public String getItemRarityName() {
        return "Common";
    }

    @Override
    public String getItemRarityColor() {
        return Color.WHITE;
    }
}
