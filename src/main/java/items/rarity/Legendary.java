package main.java.items.rarity;

import main.java.consolehelpers.Color;
import main.java.items.rarity.abstractions.Rarity;

import static main.java.basestats.ItemRarityModifiers.LEGENDARY_RARITY_MODIFIER;

/**
 * Legendary rarity, you got the best, what to do now?
 */
public class Legendary implements Rarity {

    @Override
    public double powerModifier() {
        return LEGENDARY_RARITY_MODIFIER;
    }

    @Override
    public String getItemRarityName() {
        return "Legendary";
    }

    @Override
    public String getItemRarityColor() {
        return Color.YELLOW;
    }
}
