package main.java.items.rarity;

import main.java.consolehelpers.Color;
import main.java.items.rarity.abstractions.Rarity;

import static main.java.basestats.ItemRarityModifiers.EPIC_RARITY_MODIFIER;

/**
 * Epic rarity, such epic, much good
 */
public class Epic implements Rarity {

    @Override
    public double powerModifier() {
        return EPIC_RARITY_MODIFIER;
    }

    @Override
    public String getItemRarityName() {
        return "Epic";
    }

    @Override
    public String getItemRarityColor() {
        return Color.MAGENTA;
    }
}
