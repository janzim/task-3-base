package main.java.items.rarity.abstractions;

/**
 * Base class for all rarity types
 */
public interface Rarity {
    /**
     * Stat modifier
     *
     * @return the power modifier stat
     */
    double powerModifier();

    /**
     * Get name of rarity
     *
     * @return the name of the rarity
     */
    String getItemRarityName();

    /**
     * Color for display purposes
     *
     * @return the item color
     */
    String getItemRarityColor();
}
