package main.java.items.rarity;

import main.java.consolehelpers.Color;
import main.java.items.rarity.abstractions.Rarity;

import static main.java.basestats.ItemRarityModifiers.RARE_RARITY_MODIFIER;

/**
 * Rare rarity, not the best, but it'l do
 */
public class Rare implements Rarity {

    @Override
    public double powerModifier() {
        return RARE_RARITY_MODIFIER;
    }

    @Override
    public String getItemRarityName() {
        return "Rare";
    }

    @Override
    public String getItemRarityColor() {
        return Color.BLUE;
    }
}
