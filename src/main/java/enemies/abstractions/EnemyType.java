package main.java.enemies.abstractions;

/**
 * Types of all enemies
 */
public enum EnemyType {
    Beast,
    Dragon,
    Elemental,
    Fiend,
    Giant,
    Humanoid,
    Orc,
    Undead
}
