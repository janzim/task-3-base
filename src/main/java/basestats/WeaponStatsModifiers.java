package main.java.basestats;

/**
 * This class serves as a single place to balance all the weapon stats
 */
public class WeaponStatsModifiers {
    // Staff
    public static final double STAFF_MAGIC_MOD = 1.3;
    // Wand
    public static final double WAND_MAGIC_MOD = 1.1;
    // Axe
    public static final double AXE_ATTACK_MOD = 1.4;
    // Dagger
    public static final double DAGGER_ATTACK_MOD = 1.2;
    // Hammer
    public static final double HAMMER_ATTACK_MOD = 1.8;
    // Mace
    public static final double MACE_ATTACK_MOD = 1.7;
    // Sword
    public static final double SWORD_ATTACK_MOD = 1.5;
    // Bow
    public static final double BOW_ATTACK_MOD = 1.8;
    // Crossbow
    public static final double CROSSBOW_ATTACK_MOD = 1.7;
    // Gun
    public static final double GUN_ATTACK_MOD = 2;
}
