package main.java.demonstrationHelpers;

import main.java.consolehelpers.Color;

/**
 * A collection of prompts to show to user to navigate through the demo
 */
public class Prompts {
    public static final String partySize = "How big a party do you want?";

    // todo more informative stuff
    public static final String characterChoice = "Choose a character:\n" +
            Color.BLUE + "(1)" + Color.RESET + "\tDruid (Support)\n\t\tAbilities: Healing Spell\n" +
            Color.BLUE + "(2)" + Color.RESET + "\tMage (Caster)\n\t\tAbilities: Damaging Spell\n" +
            Color.BLUE + "(3)" + Color.RESET + "\tPaladin (Melee)\n\t\tAbilities: Blunt Attack\n" +
            Color.BLUE + "(4)" + Color.RESET + "\tPriest (Support)\n\t\tAbilities: Shield Spell\n" +
            Color.BLUE + "(5)" + Color.RESET + "\tRanger (Ranged)\n\t\tAbilities: Range Attack\n" +
            Color.BLUE + "(6)" + Color.RESET + "\tRogue (Melee)\n\t\tAbilities: Blade Attack\n" +
            Color.BLUE + "(7)" + Color.RESET + "\tWarlock (Caster)\n\t\tAbilities: Damaging Spell\n" +
            Color.BLUE + "(8)" + Color.RESET + "\tWarrior (Melee)\n\t\tAbilities: Blade Attack";

    public static final String gameLoopChoices = "Choose an action:\n" +
            Color.BLUE + "(1)" + Color.RESET + "\tPerform ability\n" +
            Color.BLUE + "(2)" + Color.RESET + "\tEquip new Armor\n" +
            Color.BLUE + "(3)" + Color.RESET + "\tEquip new Weapon";

    public static final String armorChoice = "Choose an armor:\n" +
            Color.BLUE + "(1)" + Color.RESET + "\tCloth\n" +
            Color.BLUE + "(2)" + Color.RESET + "\tLeather\n" +
            Color.BLUE + "(3)" + Color.RESET + "\tMail\n" +
            Color.BLUE + "(4)" + Color.RESET + "\tPlate\n";

    public static final String weaponChoice = "Choose a weapon:\n" +
            Color.BLUE + "(1)" + Color.RESET + "\tStaff (Magic Weapon)\n" +
            Color.BLUE + "(2)" + Color.RESET + "\tWand (Magic Weapon)\n" +
            Color.BLUE + "(3)" + Color.RESET + "\tAxe (Blade Weapon)\n" +
            Color.BLUE + "(4)" + Color.RESET + "\tDagger (Blade Weapon)\n" +
            Color.BLUE + "(5)" + Color.RESET + "\tHammer (Blunt Weapon)\n" +
            Color.BLUE + "(6)" + Color.RESET + "\tMace (Blunt Weapon)\n" +
            Color.BLUE + "(7)" + Color.RESET + "\tSword (Blade Weapon)\n" +
            Color.BLUE + "(8)" + Color.RESET + "\tBow (Ranged Weapon)\n" +
            Color.BLUE + "(9)" + Color.RESET + "\tCrossbow (Ranged Weapon)\n" +
            Color.BLUE + "(10)" + Color.RESET + "Gun (Ranged Weapon)";
}
