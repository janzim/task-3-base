package main.java.demonstrationHelpers;

import main.java.characters.*;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.CharacterType;
import main.java.characters.abstractions.DamageType;
import main.java.consolehelpers.Color;
import main.java.factories.ArmorFactory;
import main.java.factories.CharacterFactory;
import main.java.factories.SpellFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.Epic;
import main.java.items.rarity.Legendary;
import main.java.items.rarity.Rare;
import main.java.items.rarity.Uncommon;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.spells.abstractions.DamagingSpell;
import main.java.spells.abstractions.HealingSpell;
import main.java.spells.abstractions.ShieldingSpell;
import main.java.spells.abstractions.SpellType;

import java.util.*;

public class Demonstration {
    List<Character> partyList;
    Random rand;

    public Demonstration() {
        partyList = new ArrayList<>();
        rand = new Random();
    }


    public void runDemo() {
        createParty(true);
        System.out.println();
        for (int i = 0; i < 4; i++) {
            Character character = partyList.get(i);
            System.out.println("Hero '" + character.getClass().getSimpleName() + "' has \t\t\tWeapon: " +
                    character.getEquippedWeapon().getClass().getSimpleName() +
                    "\t\tArmor: " + character.getEquippedArmor().getClass().getSimpleName() +
                    "\t\tRarities: " + character.getEquippedArmor().getRarityModifier().getItemRarityColor() +
                    character.getEquippedArmor().getRarityModifier().getItemRarityName() + Color.RESET);
        }

        System.out.println();
        for (int i = 0; i < 4; i++) {
            Character character = partyList.get(i);
            performAbility(character);
        }

        System.out.println();
        for (int i = 0; i < 4; i++) {
            Character character = partyList.get(i);
            takeDamage(character);
        }

        System.out.println();
        for (int i = 0; i < 4; i++) {
            Character character = partyList.get(i);
            equipArmor(character, true);
        }

        System.out.println();
        for (int i = 0; i < 4; i++) {
            Character character = partyList.get(i);
            equipWeapon(character, true);
        }

        System.out.println();
        for (int i = 0; i < 4; i++) {
            Character character = partyList.get(i);
            performAbility(character);
        }

        System.out.println();
        for (int i = 0; i < 4; i++) {
            Character character = partyList.get(i);
            takeDamage(character);
        }


        // Comment above and uncomment below for an actual gameloop


        createParty(false);
        gameLoop();
    }

    /**
     * A gameloop to go through each characters turn and let them do an action each turn
     */
    private void gameLoop() {
        Character characterTurn;
        while (true) {
            /// move in turns
            for (int i = 0; i < partyList.size(); i++) {
                characterTurn = partyList.get(i);
                System.out.println("It is now " + characterTurn.getClass().getSimpleName() + "'s turn");

                int choice = promptUser(Prompts.gameLoopChoices, 3);

                switch (choice) {
                    case 1:
                        performAbility(characterTurn);
                        takeDamage(characterTurn);
                        break;
                    case 2:
                        equipArmor(characterTurn, false);
                        break;
                    case 3:
                        equipWeapon(characterTurn, false);
                        break;
                }
            }
        }
    }

    /**
     * Equip new armor for character
     *
     * @param character    the character which should equip the armor
     * @param autoGenerate true if character should get their fitting armor automatically, false if wanting user input
     */
    private void equipArmor(Character character, boolean autoGenerate) {
        boolean equipSuccess = false;

        do {
            int armorChoice = autoGenerate ? 0 : promptUser(Prompts.armorChoice, 4);
            switch (armorChoice) {
                // case 0 is for autogenerating a fitting armor for character
                case 0:
                    equipSuccess = character.equipArmor(ArmorFactory.getArmor(ArmorType.valueOf(character.getEquippedArmor().getClass().getSimpleName()), new Rare()));
                    break;
                case 1:
                    equipSuccess = character.equipArmor(ArmorFactory.getArmor(ArmorType.Cloth, new Rare()));
                    break;
                case 2:
                    equipSuccess = character.equipArmor(ArmorFactory.getArmor(ArmorType.Leather, new Rare()));
                    break;
                case 3:
                    equipSuccess = character.equipArmor(ArmorFactory.getArmor(ArmorType.Mail, new Rare()));
                    break;
                case 4:
                    equipSuccess = character.equipArmor(ArmorFactory.getArmor(ArmorType.Plate, new Rare()));
                    break;
            }
            if (!equipSuccess) {
                System.out.println("Hero '" + character.getClass().getSimpleName() + "' cannot equip that armor type.\n" +
                        "Only " + character.getEquippedArmor().getClass().getSimpleName());
            }
        } while (!equipSuccess);

        System.out.println("Hero '" + character.getClass().getSimpleName() + "' equipped new " +
                character.getEquippedArmor().getClass().getSimpleName() + " armor with " +
                character.getEquippedArmor().getRarityModifier().getItemRarityColor() +
                character.getEquippedArmor().getRarityModifier().getItemRarityName() + Color.RESET + " rarity.");
    }

    /**
     * Equip new weapon for character
     *
     * @param character    the character which should equip the weapon
     * @param autoGenerate true if character should get their fitting weapon automatically, false if wanting user input
     */
    void equipWeapon(Character character, boolean autoGenerate) {
        boolean equipSuccess = false;

        do {
            int weaponChoice = autoGenerate ? 0 : promptUser(Prompts.weaponChoice, 10);
            switch (weaponChoice) {
                // case 0 is for autogenerating a fitting weapon for character
                case 0:
                    equipSuccess = character.equipWeapon(WeaponFactory.getItem(WeaponType.valueOf(character.getEquippedWeapon().getClass().getSimpleName()), new Legendary()));
                    break;
                case 1:
                    equipSuccess = character.equipWeapon(WeaponFactory.getItem(WeaponType.Staff, new Uncommon()));
                    break;
                case 2:
                    equipSuccess = character.equipWeapon(WeaponFactory.getItem(WeaponType.Wand, new Uncommon()));
                    break;
                case 3:
                    equipSuccess = character.equipWeapon(WeaponFactory.getItem(WeaponType.Axe, new Uncommon()));
                    break;
                case 4:
                    equipSuccess = character.equipWeapon(WeaponFactory.getItem(WeaponType.Dagger, new Uncommon()));
                    break;
                case 5:
                    equipSuccess = character.equipWeapon(WeaponFactory.getItem(WeaponType.Hammer, new Uncommon()));
                    break;
                case 6:
                    equipSuccess = character.equipWeapon(WeaponFactory.getItem(WeaponType.Mace, new Uncommon()));
                    break;
                case 7:
                    equipSuccess = character.equipWeapon(WeaponFactory.getItem(WeaponType.Sword, new Uncommon()));
                    break;
                case 8:
                    equipSuccess = character.equipWeapon(WeaponFactory.getItem(WeaponType.Bow, new Uncommon()));
                    break;
                case 9:
                    equipSuccess = character.equipWeapon(WeaponFactory.getItem(WeaponType.Crossbow, new Uncommon()));
                    break;
                case 10:
                    equipSuccess = character.equipWeapon(WeaponFactory.getItem(WeaponType.Gun, new Uncommon()));
                    break;
            }
            if (!equipSuccess) {
                System.out.println("Hero '" + character.getClass().getSimpleName() + "' cannot equip that weapon type.\n" +
                        "Only " + character.getEquippedWeapon().getClass().getSuperclass().getSimpleName());
            }
        } while (!equipSuccess);

        System.out.println("Hero '" + character.getClass().getSimpleName() + "' equipped new " +
                character.getEquippedWeapon().getClass().getSimpleName() + " weapon with " +
                character.getEquippedWeapon().getRarity().getItemRarityColor() +
                character.getEquippedWeapon().getRarity().getItemRarityName() + Color.RESET + " rarity.");
    }

    /**
     * Make the character take magic and melee damage
     *
     * @param character the character to receive the damage
     */
    void takeDamage(Character character) {
        double magicDamage = 50;
        double meleeDamage = 50;

        System.out.println(Color.RED + "Enemy" + Color.RESET + " tries to do " + Color.CYAN + magicDamage +
                Color.RESET + " magic damage and " + Color.YELLOW + meleeDamage + Color.RESET + " melee damage");

        System.out.println("Hero '" + character.getClass().getSimpleName() + "' took " + Color.CYAN +
                character.takeDamage(magicDamage, DamageType.Magic) + Color.RESET + " magic damage and " +
                Color.YELLOW + character.takeDamage(meleeDamage, DamageType.Physical) + Color.RESET + " melee damage");
    }

    /**
     * Make the character perform its special ability
     *
     * @param character the character to perform ability
     */
    void performAbility(Character character) {
        if (character instanceof Druid) {
            double healed = ((Druid) character).healPartyMember();
            // todo let user decide who to heal
            System.out.println("'Druid' Healed party member for " + Color.RED + healed + Color.RESET + " health.");
        } else if (character instanceof Mage) {
            double damaged = ((Mage) character).castDamagingSpell();
            System.out.println("'Mage' casted Damaging Spell for " + Color.CYAN + damaged + Color.RESET + " damage.");
        } else if (character instanceof Paladin) {
            double damaged = ((Paladin) character).attackWithBluntWeapon();
            System.out.println("'Paladin' did Blunt Attack for " + Color.YELLOW + damaged + Color.RESET + " damage.");
        } else if (character instanceof Priest) {
            double shielded = ((Priest) character).shieldPartyMember(100);
            // todo let user decide who to heal
            System.out.println("'Priest' Shielded party member for " + Color.RED + shielded + Color.RESET + " health.");
        } else if (character instanceof Ranger) {
            double damaged = ((Ranger) character).attackWithRangedWeapon();
            System.out.println("'Ranger' did Ranged Attack for " + Color.YELLOW + damaged + Color.RESET + " damage.");
        } else if (character instanceof Rogue) {
            double damaged = ((Rogue) character).attackWithBladedWeapon();
            System.out.println("'Rogue' did Blade Attack for " + Color.YELLOW + damaged + Color.RESET + " damage.");
        } else if (character instanceof Warlock) {
            double damaged = ((Warlock) character).castDamagingSpell();
            System.out.println("'Warlock' casted Damaging Spell for " + Color.CYAN + damaged + Color.RESET + " damage.");
        } else if (character instanceof Warrior) {
            double damaged = ((Warrior) character).attackWithBladedWeapon();
            System.out.println("'Warrior' did Blade Attack for " + Color.YELLOW + damaged + Color.RESET + " damage.");
        }
    }

    /**
     * Create characters and add to a party list
     *
     * @param autoGenerate true to generate a party of 4 random characters, false if wanting user to choose themselves
     */
    private void createParty(boolean autoGenerate) {
        int partySize = autoGenerate ? 4 : promptUser(Prompts.partySize, 4);

        for (int i = 0; i < partySize; i++) {
            int characterChoice = autoGenerate ? rand.nextInt(8) + 1 : promptUser(Prompts.characterChoice, 8);
            Character character = null;

            switch (characterChoice) {
                case 1:
                    character = new Druid((HealingSpell) SpellFactory.getSpells(SpellType.Regrowth));
                    break;
                case 2:
                    character = new Mage((DamagingSpell) SpellFactory.getSpells(SpellType.ArcaneMissile));
                    break;
                case 3:
                    character = new Paladin();
                    break;
                case 4:
                    character = new Priest((ShieldingSpell) SpellFactory.getSpells(SpellType.Barrier));
                    break;
                case 5:
                    character = new Ranger();
                    break;
                case 6:
                    character = new Rogue();
                    break;
                case 7:
                    character = new Warlock((DamagingSpell) SpellFactory.getSpells(SpellType.ChaosBolt));
                    break;
                case 8:
                    character = new Warrior();
                    break;
                default:
                    System.out.println("Something went wrong, retrying prompt");
                    i--;
                    break;
            }

            System.out.println("Hero '" + character.getClass().getSimpleName() + "' added to party");
            partyList.add(character);
        }
    }

    /**
     * Display a prompt to user and wait for their number input given a range
     *
     * @param prompt     the message to display to user
     * @param maxChoices the choices (1-maxChoices) to allow the user to operate in
     * @return the choice number the user wrote
     */
    private int promptUser(String prompt, int maxChoices) {
        Scanner sc = new Scanner(System.in);
        System.out.print("\n" + prompt + "\nChoice: ");
        String input = sc.nextLine();

        // check if input is number and if within bounds 1 - maxChoices
        while (!isNumber(input) || Integer.parseInt(input) < 1 || Integer.parseInt(input) > maxChoices) {
            System.out.println("Wrong input! Number needs to be between 1 and " + maxChoices);
            System.out.print("\n" + prompt + "\nChoice: ");
            input = sc.nextLine();
        }

        return Integer.parseInt(input);
    }

    /**
     * Check if the given string is a number
     *
     * @param test the number to test
     * @return true if number, else false
     */
    private boolean isNumber(String test) {
        try {
            Integer.parseInt(test);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }
}