package main.java.encounter;
// Imports

import main.java.enemies.abstractions.Enemy;

import java.util.ArrayList;

/**
 * This class is responsible for generating the encounters at each floor.
 */
public class Encounter {
    // A list to keep track of the enemies in this encounter
    ArrayList<Enemy> enemies;
}
