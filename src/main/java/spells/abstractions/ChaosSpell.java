package main.java.spells.abstractions;

public interface ChaosSpell extends Spell {
    double getChaosAmount();
}
